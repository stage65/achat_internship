import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from  'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class FactureService {
  private apiiUrlFacture =  environment.apiiUrlFacture;

  readonly API_URL = this.apiiUrlFacture
  
  // readonly API_URL = 'http://oumaima.api.local/SpringMVC/facture';

  constructor(private httpClient: HttpClient) { }

  getAllFactures() {
    return this.httpClient.get(`${this.API_URL}/retrieve-all-factures`)
  }
  addFacture(facture : any) {
    return this.httpClient.post(`${this.API_URL}/add-facture`, facture)
  }
}
