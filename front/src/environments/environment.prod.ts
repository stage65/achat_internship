export const environment = {
  production: true,
  apiiUrl: 'http://oumaima.api.local/SpringMVC/operateur',
  apiiUrlFacture:  'http://oumaima.api.local/SpringMVC/facture',
  apiiUrlProduit: 'http://oumaima.api.local/SpringMVC/produit',
  apiiUrlStock: 'http://oumaima.api.local/SpringMVC/stock'
};
